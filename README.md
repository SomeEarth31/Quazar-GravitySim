# Quazar-Project: Gravity/Electron Simulator
This repository contains a program (written in C++) that analyzes an N-body system. It takes in a CSV 
file with initial conditionsin the following format: 
mass, x, y, z, Vx, Vy, Vz, q.

Where (x, y, z) are the coordinates of the body, (Vx, Vy, Vz) are its velocity 
components, q is its charge, and mass is the mass.The other inputs taken by 
the program are choosing between the gravity or electric simulators(g/e) and 
then inputting the time of analysis(ex. 400 days in the gravity simulator).
Both of these are terminal inputs. Upon completion, the program outputs N+1 
CSV files. N CSV files contain the position and velocity of each body after
every calculation. The last CSV file contains the position of every body 
after each calculation set. This last file, called “data.tmp” can then be 
used to graph the path taken by each body. The program also output the time 
taken to run the complete analysis and the final positions and velocities 
of each body(terminal outputs).This is in the following format: 
(x, y, z) | (Vx, Vy, Vz). 

Instructions on how to run the simulation: Get the data for the planatary bodies and put it into a csv file, following the format given above. For the solar system, a file called "settings.txt" is given in the resources folder. Choose your input method:
1. Manual Input: Use the file names NbodyIn.cpp. Rename your csv as "settings.txt" and place it in the same folder as the       cpp file. Run the program by inputting the following commands:

            > g++ NbodyIn.cpp -o NbodyIn
            > ./NbodyIn
2. Command Line Input: Use the file named NbodyCmd.cpp. Place your csv file and the .cpp in the same folder. To run the program use the follwing commands:

        #To compile
        > g++ NbodyIn.cpp -o NbodyIn
        #For Gravity Simulator
        > ./NbodyIn -g <name_of_csv_file> <time_of_measurement(in days)> <time_step(in seconds)>
        //For Elctrostatic Sim

        >./NbodyIn -g <name_of_csv_file> <time_of_measurement(in 10e-20s)>  
3. Bonus: For easier readibility there is a version the code broken up into multiple .h and .cxx files. That is kept in the Code_M folder. To run the program extract the contents of the .zip file into a folder. If you are using your own csv file, delete or move the "settings.txt" file. Then run the following commands:

        #To compile
        >g++ Vector.cxx Particle.cxx Simulation_Engine.cxx
        #For Gravity Simulator
        > ./a.out -g <name_of_csv_file> <time_of_measurement(in days)> <time_step(in seconds)>
        #For Elctrostatic Sim
        >./a.out -g <name_of_csv_file> <time_of_measurement(in 10e-20s)>
        
The file labelled "documentation.zip" contains the doxygen documentation. Click on the file named "index.html" to access the HTML. The resources folder contains the data taken from JPL Horizon's Email bot. 
